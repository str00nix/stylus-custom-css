youtubus deletus.css

- Main feature is that no suggested videos are displayed on the main page.
- The annoying +5 second and -5 second playback popups on the video screen are hidden.
- The placeholder for writing a comment is removed so it won't be accidentally pressed for it to redirect the user to log in.

---

youtubus deletus 2.css

- Mainly to hide video suggestions on the right side of the video.

---

developer.mozilla.org.css

- Dark mode for MDN Web Docs before they added their own. Now this one should give a lighter theme than their official one.